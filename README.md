Übungszettel 9
==============

Pushen Sie nach der Bearbeitung jeder Aufgabe Ihre Ergebnisse. 

Issue 1
-------
a) Führen Sie in Ihrem Übungsverzeichnis die folgenden Befehle aus. 

Yesod-Grundgerüst initialisieren:

```bash
$ stack new NAMEIHRESWEBPROJEKTS --bare yesod-sqlite
```

Kompilieren mit yesod: 
```bash
$ stack build yesod-bin
```

Starten des Servers:
```bash
$ stack exec -- yesod devel
```

Öffnen Sie einen Browser mit der Domain `http://localhost:3000/`. Wenn alles geklappt hat, sollten Sie hier bereits die Möglichkeit haben, sich mit einem Dummy-Account einzuloggen und eine Profilseite zu öffnen. 

b) In `config/models` werden alle Entitäten definiert, die Ihre Datenbank kennen soll. Diese soll nun im Hinblick auf Issue 2-4 angepasst werden. Die dafür nötigen Entitäten sind in einem [Entity-relationship model](https://de.wikipedia.org/wiki/Entity-Relationship-Modell) festgehalten, welches Sie in der Datei [ERModel.pdf](/ERModel.pdf) finden. 

- Verschieben Sie das mitgegebene Modul `MyTypes.hs` in den `src/`-Ordner und importieren Sie es in der `Model.hs`

- Pflegen Sie die Entitäten aus [ERModel.pdf](/ERModel.pdf) sinnvoll in `config/models` ein. 

Hinweise: 

- Für jede Entität werden automatisch Id-Typen definiert (z.B. `UserId` für die Entität `User`). 

- Das Attribut `ident` ist der Name des Nutzers. 

- Wenn Sie selber definierte Typen verwenden wollen, um die Ausprägungen eines Entity-Feldes festzulegen, definieren Sie diese in der mitgegebenen Datei `MyTypes.hs`. 

- Für textuelle Felder, nutzen Sie den Typ `Text`. Falls ein Feld auch leer sein darf, fügen Sie ein `Maybe` hinzu. 

Issue 2
-------

Neues Feature: Timeline für alle Nutzer. Auf der Seite `All Users` sollen alle jemals eingelogten Nutzer angezeigt werden. Mit einem Klick auf einen Nutzernamen wird man auf die zugehörige `Timeline` weitergeleitet. Auf der Timeline muss erst einmal nichts weiter stehen außer der Titel: "USERNAME's Timeline". 

- `config/routes`: Erstellen Sie die folgenden Routen: 

```
/timeline/#UserId UserTimelineR GET
/userList UserListR GET
```

- `Foundation.hs`: 

   - Ergänzen Sie die Funktionen, in denen auf Routen-Konstruktoren pattern-gematcht wird. 
   - Die Routen `UserListR` und `UserTimelineR` sollen nur für autorisierte (d.h. eingeloggte) Nutzer sichtbar sein. 
   - Sorgen Sie dafür, dass `UserListR` über den Menüpunkt "All Users" erreichbar ist (siehe MenuItem in defaultLayout).
   
- `src/Handler/`: 
   - Kopieren Sie die vorgegebenen Handler-Module `UserTimeline.hs` und `UserList.hs` hier hin.
- `templates/`: Man kann hamlet-Code für Widgets etc. auch in template-Files auslagern. Diese werden hier gespeichert. 
   - Kopieren Sie die vorgegebenen Template-Dateien `UserTimeline.hamlet` und `UserList.hamlet` hier hin.
-  `.cabal`: Fügen Sie die Handler als `exposed-module` hinzu. 
- `src/Application.hs`: Importieren Sie die neuen Handler. 
- `src/Handler/UserList.hs`/`src/Handler/UserTimeline.hs` bzw. `templates/UserTimeline.hamlet`/`templates/UserList.hamlet`:

   - Bearbeiten Sie die TODOs zu Issue 2. 

Issue 3
-------


Neues Feature: Posten auf der eigenen Timeline. Jeder Nutzer soll nun auf seiner eigenen `Timeline`-Seite Nachrichten mit anderen teilen können. 
- Legen Sie hierfür in `config/routes` die neue Route an: 


```
/timeline TimelineR GET POST
```

- Kopieren Sie `Timeline.hs` und `timeline.hamlet` ins Handler- bzw. Templates-Verzeichnis.
- Die Route `TimelineR` soll für autorisierte Nutzer über einen extra Menüpunkt "Your Timeline" erreichbar sein. Führen Sie die nötigen Änderungen in `.cabal`, `Foundation.hs` und `Application.hs` durch. 
- Bearbeiten Sie die Aufgaben in `Timeline.hs` und `timeline.hamlet`. 

Issue 4
-------

Never gonna give U up, never gonna let ya down – Friendship :D

Die Seite `All Users` soll nun auch erlauben, Freundschaftsanfragen abzuschicken, abzulehnen. Auf dieser soll man einem Nutzer eine Freundschaftsanfrage stellen können. Es soll erkenntlich sein, mit wem man bereits befreundet ist. Man soll nur die Timelines befreundeter Nutzer besuchen können. 
Implementieren Sie einen `postUserlistR`-Funktion, die dies erledigt. Um Information an POST zu senden, können Sie eine Form mit einem `hiddenField` einsetzen. In diesem `hiddenField` können Sie dann die im Falle des Aufrufs von `postUserlistR` gewünschte Information hinterlegen. 

Beispiel: 

```haskell
hiddenUIDForm :: UserId -> Form UserId
hiddenUIDForm uid = renderDivs $ areq hiddenField "" (Just uid)
```


ZETTEL 10
=========

Issue 1
-------
a) Implementieren Sie einen Login über Github. Github bietet Ihnen eine sogenannte OAuth Schnittstelle, durch die Sie sicher die Authentizität eines Benutzers überprüfen können.

In dem packet `yesod-auth-oauth2` ist bereits ein Github-OAuth PLugin vorhanden, dass Sie nun noch laden müssen. Falls Sie auf Github keine eigene Application erstellen wollen, können Sie die unten angegebenen Daten verwenden, um Ihre Application gegenüber Github zu authentifizieren.

```
Client ID
b52908be7f1c8b1d763f
Client Secret
15d7961bb26c7b4791999847aec81c5c125ec688
```
Diese leitet nach der Authentifizierung nach http://localhost:3000/ weiter.

b) Nachdem ein User sich nun per Github einloggen kann, sollten Sie dafür sorgen, dass der Github-Benutzername sowie die Mail Adresse ausgelesen und gespeichert werden.
Eventuell finden Sie folgende Resourcen interessant, wenn Sie nicht Wissen wo diese Daten abzufragen sind:

[Yesod.Auth.Creds](https://hackage.haskell.org/package/yesod-auth-1.4.17.2/docs/Yesod-Auth.html#t:Creds)

[Yesod.Auth.OAuth2.Github.toCreds](https://github.com/thoughtbot/yesod-auth-oauth2/blob/master/Yesod/Auth/OAuth2/Github.hs#L99)


Issue 2
-------
Es nutzen immer weniger Nutzer Ihren Social-Hub! Tag für Tag werden es weniger! Was nun?

#dasGehtBesser #sieWissenWasAufSieZuKommt #alleGutenDingeSindDrei

User wollen Hashtags!

a) Schreiben Sie einen Parser, der aus einem Text alle Wörter, die mit einem # beginnen,
in eine Liste parst. Wenden Sie diesen Parser auf jeden Post, der neu erstellt wird, an.

Da Yesod mit dem Datentyp Text arbeitet, sollten Sie statt `Data.Attoparsec.ByteString`
besser [Data.Attoparsec.Text](http://hackage.haskell.org/package/attoparsec-0.13.1.0/docs/Data-Attoparsec-Text.html) verwenden.

b) Nun müssen diese Tags noch persistent gespeichert werden. Erweitern Sie hierzu Ihr
Datenbankmodell. (Einen Vorschlag, wie das Aussehen könnte, finden Sie in der Datei
ER_II.pdf)


Issue 3
-------
Ihre Nutzer kommen immer noch nicht wieder....

Und plötzlich fällt ihnen ein warum! Man kann ja noch nicht nach Tags suchen!

a)
Legen Sie eine neue Seite an (Hinweise hierzu finden Sie auf Übungszettel 9 Issue 2),
die später als Suchseite für getaggte Inhalte genutzt werden soll. Diese Seite solte
nur für eingeloggte User erreichbar sein und im Menü sollte ein Link zu dieser Seite
zu finden sein.

b)
Lassen Sie nun die Tags in den Posts als Link anzeigen, der auf diese Suchseite verlinkt.
Auf der Suchseite sollte nun auch der Name des Tags zu lesen sein, auf den
geklickt worden ist.

https://www.yesodweb.com/book/routing-and-handlers#routing-and-handlers_types_of_pieces

Issue 4
-------

Implementieren Sie nun die Suchfunktion dieser Seite.

- Wenn man auf einen Tag geklickt hat soll die Ergebnisseite der Suche erscheinen (GET).
- Auf dieser Seite sollte ein Eingabefeld sein, mit dem man nach Tags suchen kann (POST).
    - Hinweis: Nachdem Sie die Gültigkeit des Formulares überprüft haben können Sie auch einen Redirect auf die GET-Seite machen um den Code nicht doppelt zu schreiben.
- Unter dem Eingabefeld sollen die Ergebnisse angezeigt werden.
- Unter dieser Anzeige sollte die Timeline des Users, der den Post verfasst hat, verlinkt sein.
