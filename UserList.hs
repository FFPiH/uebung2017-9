{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Handler.UserList where

import Import

-- Die Typspezifikation von userlist ist nur notwendig, 
-- weil im Folgenden noch keine Verwendung von userlist implementiert ist, 
-- aus welcher der TypeChecker den Typ schließen könnte. 
-- TODO: (Issue 2) Bearbeiten Sie die Aufgaben im template "userList". 
getUserListR :: Handler Html
getUserListR = do
    (userlist :: [Entity User]) <- runDB $ selectList [] [] 
    defaultLayout $ do
        setTitle . toHtml $ ("All Users" :: Text) -- setzt den Titel des Browserfensters 
        $(widgetFile "userList")


--TODO: Issue 4
postUserListR :: Handler Html
postUserListR = undefined