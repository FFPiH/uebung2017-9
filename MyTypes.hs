-- @Employment.hs
{-# LANGUAGE TemplateHaskell #-}
module MyTypes where

import Database.Persist.TH
import Prelude

data FSStatus = undefined
    deriving (Show, Read, Eq)
derivePersistField "FSStatus"