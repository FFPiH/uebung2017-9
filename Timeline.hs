{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Handler.Timeline where

import Import

-- TODO: Bearbeiten Sie die Aufgaben im Widget-Template "timeline"
getTimelineR :: Handler Html
getTimelineR = do 
    (uid, user) <- requireAuthPair
    posts <- runDB $ selectList [PostAuthor ==. uid] [Desc PostCreated]
    (postWidget, postEnctype) <- generateFormPost (postForm Nothing uid)
    let userName :: Text
        userName = userIdent user
    defaultLayout $ do
        setTitle . toHtml $ userName <> "'s Timeline"
        $(widgetFile "timeline")

-- TODO: Implementieren Sie ein Verhalten zum Speichern des Posts. 
--       Nützliche Funktionen: runFormPost. 
postTimelineR :: Handler Html
postTimelineR = do
    getTimelineR

-- TODO: Eventuell müssen Sie hier oder in config/models die Reihenfolge der Post-Argumente anpassen. 
postForm :: UserId -> Form Post
postForm u = renderDivs $ Post
                              -- UserId
                         <$> pure u
                              -- Textfeld mit Default-Wert "Post content"
                         <*> areq textField "Post" (Just "Post content")
                              -- Systemzeit
                         <*> lift (liftIO getCurrentTime)
