{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.UserTimeline where

import Import

getUserTimelineR :: UserId -> Handler Html
getUserTimelineR uid = do 
    mayUser <- runDB $ get uid
    posts <- runDB $ selectList [PostAuthor ==. uid] [Desc PostCreated]
    let userName :: Text
        userName = maybe "UnkownUser" userIdent mayUser
    defaultLayout $ do
        setTitle . toHtml $ show uid <> "'s Timeline"
        $(widgetFile "userTimeline")
